const templateModel = require("../models/templateModel");
const imageModel = require("../models/imageModel");

const {
  requestTranform,
  responseTranform,
} = require("../transformers/templateTransformer");

class templateController {
  static find = async (req, res) => {
    try {
      const templates = await templateModel.find();
      const templatesTranformed = this.transformData(templates);
      res.status(200).json(templatesTranformed);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static insertOne = async (req, res) => {
    try {
      let newTemplate = req.body;
      newTemplate.active = false;
      newTemplate.isPremium = false;
      newTemplate = requestTranform(newTemplate);
      const templates = await templateModel.insertOne(newTemplate);
      const templatesTranformed = this.transformData(templates);
      res.status(200).json(templatesTranformed[0]);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static findById = async (req, res) => {
    try {
      const templates = await templateModel.findOne(req.params.templateId);
      const templatesTranformed = this.transformData(templates);
      res.status(200).json(templatesTranformed[0]);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static findOneAndDelete = async (req, res) => {
    try {
      const template = await templateModel.findOneAndDelete(
        req.params.templateId
      );
      const templateTranformed = responseTranform(template.value);
      res.status(200).json(templateTranformed);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static findOneAndUpdate = async (req, res) => {
    try {
      const newTemplate = requestTranform(req.body);
      const templates = await templateModel.findOneAndUpdate(
        req.params.templateId,
        newTemplate
      );
      imageModel.insertMany(newTemplate.images);
      const templatesTranformed = this.transformData(templates);
      res.status(200).json(templatesTranformed[0]);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static transformData = (templates) => {
    var templatesTransformed = [];
    for (let i = 0; i < templates.length; i++) {
      templatesTransformed.push(responseTranform(templates[i]));
    }
    return templatesTransformed;
  };
}

module.exports = templateController;

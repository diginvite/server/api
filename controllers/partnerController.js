const partnerModel = require("../models/partnerModel");
const {
  requestTranform,
  responseTranform,
} = require("../transformers/partnerTransformer");

class partnerController {
  static find = async (req, res) => {
    try {
      const partners = await partnerModel.find();
      const partnersTransformed = responseTranform(partners);
      res.status(200).json(partnersTransformed);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static insertOne = async (req, res) => {
    try {
      const newPartner = requestTranform({ ...req.body, active: false });
      const partner = await partnerModel.insertOne(newPartner);
      const partnerTransformed = responseTranform(partner.ops[0]);
      res.status(200).json(partnerTransformed);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static findById = async (req, res) => {
    try {
      const partner = await partnerModel.findOne(req.params.partnerId);
      const partnerTransformed = responseTranform(partner);
      res.status(200).json(partnerTransformed);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static findOneAndUpdate = async (req, res) => {
    try {
      const newPartner = requestTranform(req.body);
      const partner = await partnerModel.findOneAndUpdate(
        req.params.partnerId,
        newPartner
      );
      const partnerTransformed = responseTranform(partner.value);
      res.status(200).json(partnerTransformed);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static findOneAndDelete = async (req, res) => {
    try {
      const partner = await partnerModel.findOneAndDelete(req.params.partnerId);
      const partnerTransformed = responseTranform(partner.value);
      res.status(200).json(partnerTransformed);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };
}

module.exports = partnerController;

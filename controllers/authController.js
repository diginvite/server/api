const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const userModel = require("../models/userModel");

class authController {
  static login = async (req, res) => {
    try {
      const user = await userModel.findByEmailOrUsername(req.body.email);
      !user && res.status(500).json({ message: "Email/username is wrong" });
      const isPassword = await bcrypt.compare(req.body.password, user.password);
      !isPassword && res.status(500).json({ message: "Password is wrong" });
      const token = jwt.sign(
        { id: user._id, username: user.username, xxx: user.username },
        process.env.JWT_SECRET_KEY
      );
      res.status(200).json({
        token,
      });
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };
}

module.exports = authController;

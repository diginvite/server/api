const songModel = require("../models/songModel");
const { responseTransform } = require("../transformers/songTransformer");

class songController {
  static async find(req, res) {
    try {
      const songs = await songModel.find();
      const songsTransformed = responseTransform(songs);
      res.status(200).json(songsTransformed);
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }

  static insertOne = async (req, res) => {
    try {
      const song = await songModel.insertOne(req.body);
      const songTransformed = responseTransform(song.ops[0]);
      res.status(200).json(songTransformed);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };
}

module.exports = songController;

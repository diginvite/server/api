const bcrypt = require("bcrypt");
const userModel = require("../models/userModel");
const {
  requestTranform,
  responseTranform,
  requestUpdateTranform,
} = require("../transformers/userTransformer");

class userController {
  static find = async (req, res) => {
    try {
      const users = await userModel.find();
      const usersTranformed = responseTranform(users);
      res.status(200).json(usersTranformed);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static insertOne = async (req, res) => {
    const isPasswordValidation = this.checkPassword(
      req.body.password,
      req.body.passwordConfirmation
    );
    !isPasswordValidation &&
      res.status(500).json({ message: "Password doest'n match" });

    const isEmail = await this.checkEmail(req.body.email);
    isEmail && res.status(500).json({ message: "Email already exist" });

    const isUsername = await this.checkUsername(req.body.username);
    isUsername && res.status(500).json({ message: "Username already exist" });

    try {
      const hashPassword = await this.hashPassword(req.body.password);
      const newUser = requestTranform({ ...req.body, password: hashPassword });
      const user = await userModel.insertOne(newUser);
      const userTranformed = responseTranform(user.ops[0]);
      res.status(200).json(userTranformed);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static findOneAndDelete = async (req, res) => {
    try {
      const user = await userModel.findOneAndDelete(req.params.userId);
      const userTranformed = responseTranform(user.value);
      res.status(200).json(userTranformed);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static findById = async (req, res) => {
    try {
      const user = await userModel.findOne(req.params.userId);
      const userTranformed = responseTranform(user);
      res.status(200).json(userTranformed);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static findOneAndUpdate = async (req, res) => {
    try {
      const newUser = requestUpdateTranform(req.body);
      const user = await userModel.findOneAndUpdate(req.params.userId, newUser);
      const userTranformed = responseTranform(user.value);
      res.status(200).json(userTranformed);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static findProfile = async (req, res) => {
    try {
      const user = await userModel.findOne(req.user.id);
      const userTranformed = responseTranform(user);
      res.status(200).json(userTranformed);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static changePassword = async (req, res) => {
    const isPasswordValidation = this.checkPassword(
      req.body.password,
      req.body.passwordConfirmation
    );

    !isPasswordValidation &&
      res.status(500).json({ message: "Password doest'n match" });

    try {
      const hashPassword = await this.hashPassword(req.body.password);
      const user = await userModel.findOneAndUpdate(req.params.userId, {
        password: hashPassword,
      });
      const userTranformed = responseTranform(user.value);
      res.status(200).json(userTranformed);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static checkPassword = (password, passwordConfirmation) => {
    if (password === passwordConfirmation) {
      return true;
    }
    return false;
  };

  static checkEmail = async (email) => {
    const user = await userModel.findByEmail(email);
    if (user) {
      return true;
    }

    return false;
  };

  static checkUsername = async (username) => {
    const user = await userModel.findByUsername(username);
    if (user) {
      return true;
    }
    return false;
  };

  static hashPassword = (plainPassword) => {
    return bcrypt.hash(plainPassword, 10);
  };
}

module.exports = userController;

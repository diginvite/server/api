const { ObjectId } = require("mongodb");

class utilityController {
  static pluck(array, key) {
    var result = [];
    for (let i = 0; i < array.length; i++) {
      if (key === "id") {
        result.push(ObjectId(array[i][key]));
      } else {
        result.push(array[i][key]);
      }
    }
    return result;
  }
}

module.exports = utilityController;

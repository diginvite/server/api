const packageModel = require("../models/packageModel");
const { pluck } = require("./utilityController");

const {
  requestTranform,
  responseTranform,
} = require("../transformers/packageTransformer");

class orderController {
  static find = async (req, res) => {
    try {
      // find any data
      const packages = await packageModel.find();
      // tranform data
      const packagesTransformed = this.transformData(packages);
      // response
      res.status(200).json(packagesTransformed);
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: error.message });
    }
  };

  static insertOne = async (req, res) => {
    try {
      // call pluck method
      const featureIds = pluck(req.body.features, "id");
      // prepare data
      let newPackage = req.body;
      newPackage.active = false;
      newPackage.featureIds = featureIds;
      // tranform for data insert
      const newPackageTransformed = requestTranform(newPackage);
      // insert data
      const packageData = await packageModel.insertOne(newPackageTransformed);
      // tranform data form response
      const packageTranformed = this.transformData(packageData);
      // response
      res.status(200).json(packageTranformed[0]);
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: error.message });
    }
  };

  static findById = async (req, res) => {
    try {
      // find one data by id
      const packageData = await packageModel.findOne(req.params.packageId);
      // tranform data form response
      const packageTranformed = this.transformData(packageData);
      // response
      res.status(200).json(packageTranformed[0]);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static findOneAndUpdate = async (req, res) => {
    try {
      // call pluck method
      const featureIds = pluck(req.body.features, "id");
      // prepare data
      let newPackage = req.body;
      newPackage.featureIds = featureIds;
      // tranform data for insert
      const newPackageTransformed = requestTranform(newPackage);
      // update data
      const packageData = await packageModel.findOneAndUpdate(
        req.params.packageId,
        newPackageTransformed
      );
      // tranform data for response
      const packageTranformed = this.transformData(packageData);
      // response
      res.status(200).json(packageTranformed[0]);
    } catch (error) {
      console.log(error);

      res.status(500).json({ message: error.message });
    }
  };

  static findOneAndDelete = async (req, res) => {
    try {
      // delete data
      const packageData = await packageModel.findOneAndDelete(
        req.params.packageId
      );
      // response
      res.status(200).json(packageData.value);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static transformData = (packages) => {
    var packagesTransformed = [];
    for (let i = 0; i < packages.length; i++) {
      packagesTransformed.push(responseTranform(packages[i]));
    }
    return packagesTransformed;
  };
}

module.exports = orderController;

const featureModel = require("../models/featureModel");
const {
  requestTranform,
  responseTranform,
} = require("../transformers/featureTransformer");

class featureController {
  static find = async (req, res) => {
    try {
      const features = await featureModel.find();
      const featuresTranformed = responseTranform(features);
      res.status(200).json(featuresTranformed);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static insertOne = async (req, res) => {
    try {
      const newFeature = requestTranform({ ...req.body, active: false });
      const feature = await featureModel.insertOne(newFeature);
      const featureTranformed = responseTranform(feature.ops[0]);
      res.status(200).json(featureTranformed);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static findById = async (req, res) => {
    try {
      const feature = await featureModel.findOne(req.params.featureId);
      const featureTranformed = responseTranform(feature);
      res.status(200).json(featureTranformed);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static findOneAndUpdate = async (req, res) => {
    try {
      const newFeature = requestTranform(req.body);
      const feature = await featureModel.findOneAndUpdate(
        req.params.featureId,
        newFeature
      );
      const featureTranformed = responseTranform(feature.value);
      res.status(200).json(featureTranformed);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static findOneAndDelete = async (req, res) => {
    try {
      const feature = await featureModel.findOneAndDelete(req.params.featureId);
      const featureTranformed = responseTranform(feature.value);
      res.status(200).json(featureTranformed);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };
}

module.exports = featureController;

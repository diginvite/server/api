const videoModel = require("../models/videoModel");
const { responseTransform } = require("../transformers/videoTransformer");

class videoController {
  static async find(req, res) {
    try {
      const videos = await videoModel.find();
      const videosTransformed = responseTransform(videos);
      res.status(200).json(videosTransformed);
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }

  static insertOne = async (req, res) => {
    try {
      const video = await videoModel.insertOne(req.body);
      const videoTransformed = responseTransform(video.ops[0]);
      res.status(200).json(videoTransformed);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };
}

module.exports = videoController;

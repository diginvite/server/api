const orderModel = require("../models/orderModel");
const imageModel = require("../models/imageModel");
const videoModel = require("../models/videoModel");
const songModel = require("../models/songModel");

const {
  requestTranform,
  responseTranform,
} = require("../transformers/orderTransformer");

class orderController {
  static find = async (req, res) => {
    try {
      const orders = await orderModel.find();
      const ordersTranformed = this.transformData(orders);
      res.status(200).json(ordersTranformed);
    } catch (error) {
      console.log(error);

      res.status(500).json({ message: error.message });
    }
  };

  static insertOne = async (req, res) => {
    const isDomain = await this.checkDomain(req.body.domain);
    isDomain && res.status(500).json({ message: "Domain already exist" });

    try {
      let newOrder = {
        ...req.body,
        active: false,
        detail: {
          couples: [],
          events: [],
          stories: [],
          quotes: [],
          images: [],
          videos: [],
          songs: [],
          couples: [],
          payments: [],
          invitations: [],
          wishes: [],
        },
      };
      newOrder = requestTranform(newOrder);
      const orders = await orderModel.insertOne(newOrder);
      const orderTranformed = this.transformData(orders);
      res.status(200).json(orderTranformed[0]);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static findById = async (req, res) => {
    try {
      const orders = await orderModel.findOne(req.params.orderId);
      const orderTranformed = this.transformData(orders);
      res.status(200).json(orderTranformed[0]);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static findOneAndDelete = async (req, res) => {
    try {
      const order = await orderModel.findOneAndDelete(req.params.orderId);
      const orderTranformed = responseTranform(order.value);
      res.status(200).json(orderTranformed);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static findOneAndUpdate = async (req, res) => {
    try {
      const newOrder = requestTranform(req.body);
      const orders = await orderModel.findOneAndUpdate(
        req.params.orderId,
        newOrder
      );
      const orderTranformed = this.transformData(orders);
      res.status(200).json(orderTranformed[0]);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static checkDomain = async (domain) => {
    const order = await orderModel.findByDomainAndActive(domain);
    if (order.length > 0) {
      return true;
    }
    return false;
  };

  static transformData = (orders) => {
    var ordersTransformed = [];
    for (let i = 0; i < orders.length; i++) {
      ordersTransformed.push(responseTranform(orders[i]));
    }
    return ordersTransformed;
  };
}

module.exports = orderController;

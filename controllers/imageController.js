const imageModel = require("../models/imageModel");
const { responseTransform } = require("../transformers/imageTransformer");

class imageController {
  static async find(req, res) {
    try {
      const images = await imageModel.find();
      const imagesTransformed = responseTransform(images);
      res.status(200).json(imagesTransformed);
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }

  static insertOne = async (req, res) => {
    try {
      const image = await imageModel.insertOne(req.body);
      const imagesTransformed = responseTransform(image.ops[0]);
      res.status(200).json(imagesTransformed);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };
}

module.exports = imageController;

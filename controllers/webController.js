const featureModel = require("../models/featureModel");
const featureTransformer = require("../transformers/featureTransformer");
const packageModel = require("../models/packageModel");
const packageController = require("../controllers/packageController");
const templateModel = require("../models/templateModel");
const templateController = require("../controllers/templateController");
const partnerModel = require("../models/partnerModel");
const partnerTransformer = require("../transformers/partnerTransformer");
const orderModel = require("../models/orderModel");
const orderTransformer = require("../transformers/orderTransformer");

class webController {
  static findFeatures = async (req, res) => {
    try {
      const features = await featureModel.findActiveData();
      const featuresTranformed = featureTransformer.responseTranform(features);
      res.status(200).json(featuresTranformed);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static findPackages = async (req, res) => {
    try {
      const packages = await packageModel.findActiveData();
      const packagesTransformed = packageController.transformData(packages);
      res.status(200).json(packagesTransformed);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static findTemplates = async (req, res) => {
    try {
      const templates = await templateModel.findActiveData();
      const templatesTranformed = templateController.transformData(templates);
      res.status(200).json(templatesTranformed);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static findPartners = async (req, res) => {
    try {
      const partners = await partnerModel.findActiveData();
      const partnersTransformed = partnerTransformer.responseTranform(partners);
      res.status(200).json(partnersTransformed);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static findOrder = async (req, res) => {
    try {
      const { domain, invitation } = req.query;
      var order = await orderModel.findByDomainAndActive(domain);
      if (order.length < 1)
        order = await orderModel.findByDomainAndActive("template");
      const orderTransformed = orderTransformer.responseTranform(order[0]);
      res.status(200).json({
        order: orderTransformed,
        invitation: invitation ? invitation : null,
      });
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  static insertOneWish = async (req, res) => {
    try {
      const { orderId } = req.params;
      const wish = req.body;
      wish.isShow = true;
      const order = await orderModel.insertOneWish(orderId, wish);
      const orderTransformed = orderTransformer.responseTranform(order[0]);
      res.status(200).json(orderTransformed);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };
}

module.exports = webController;

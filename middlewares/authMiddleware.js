const jwt = require("jsonwebtoken");
const userModel = require("../models/userModel");

class authMiddleware {
  static authenticateToken = (req, res, next) => {
    const authHeader = req.headers.authorization;
    const token = authHeader && authHeader.split(" ")[1];
    if (token === null) return res.sendStatus(401);

    jwt.verify(token, process.env.JWT_SECRET_KEY, function (err, user) {
      if (err) return res.status(403).json(err);
      req.user = user;
      next();
    });
  };

  static authenticateUser = async (req, res, next) => {
    const user = await userModel.findByEmailOrUsername(req.user.username);
    if (!user) return res.sendStatus(403);
    req.user = { ...req.user, id: String(user._id) };
    next();
  };
}

module.exports = authMiddleware;

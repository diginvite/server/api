const db = require("../configs/mongo");
const Video = db.collection("videos");

class videoModel {
  static async insertMany(newVideos) {
    var videos = [];
    for (let i = 0; i < newVideos.length; i++) {
      const videoChecker = await this.findByUrl(newVideos[i].url);
      if (videoChecker === null) {
        const newVideo = {
          url: newVideos[i].url,
          thumbnailUrl: newVideos[i].thumbnailUrl,
          path: newVideos[i].path,
        };
        const video = await this.insertOne(newVideo);
        videos.push(video.ops[0]);
      } else {
        videos.push(videoChecker);
      }
    }
    return videos;
  }

  static insertOne(newVideo) {
    return Video.insertOne(newVideo);
  }

  static find() {
    return Video.find().toArray();
  }

  static findByUrl(url) {
    return Video.findOne({ url: url });
  }
}

module.exports = videoModel;

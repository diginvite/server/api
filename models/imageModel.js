const db = require("../configs/mongo");
const Image = db.collection("images");

class imageModel {
  static async insertMany(newImages) {
    var images = [];
    for (let i = 0; i < newImages.length; i++) {
      const imageChecker = await this.findByUrl(newImages[i].url);
      if (imageChecker === null) {
        const newImage = {
          url: newImages[i].url,
          thumbnailUrl: newImages[i].thumbnailUrl,
          path: newImages[i].path,
        };
        const image = await this.insertOne(newImage);
        images.push(image.ops[0]);
      } else {
        images.push(imageChecker);
      }
    }
    return images;
  }

  static insertOne(newImage) {
    return Image.insertOne(newImage);
  }

  static find() {
    return Image.find().toArray();
  }

  static findByUrl(url) {
    return Image.findOne({ url: url });
  }
}

module.exports = imageModel;

const db = require("../configs/mongo");
const Feature = db.collection("features");
const { ObjectId } = require("mongodb");

class featureModel {
  static find = () => {
    return Feature.find({}).toArray();
  };

  static findOne = (id) => {
    return Feature.findOne({ _id: ObjectId(id) });
  };

  static findActiveData = () => {
    return Feature.find({ active: true }).toArray();
  };

  static insertOne = (newUser) => {
    return Feature.insertOne(newUser);
  };

  static findOneAndDelete = (id) => {
    return Feature.findOneAndDelete({ _id: ObjectId(id) });
  };

  static findOneAndUpdate = (id, newFeature) => {
    return Feature.findOneAndUpdate(
      { _id: ObjectId(id) },
      { $set: newFeature },
      {
        returnOriginal: false,
      }
    );
  };
}

module.exports = featureModel;

const db = require("../configs/mongo");
const User = db.collection("users");
const { ObjectId } = require("mongodb");

class userModel {
  static find = () => {
    return User.find({}).toArray();
  };

  static findOne = (id) => {
    return User.findOne({ _id: ObjectId(id) });
  };

  static insertOne = (newUser) => {
    return User.insertOne(newUser);
  };

  static findOneAndDelete = (id) => {
    return User.findOneAndDelete({ _id: ObjectId(id) });
  };

  static findOneAndUpdate = (id, newUser) => {
    return User.findOneAndUpdate(
      { _id: ObjectId(id) },
      { $set: newUser },
      {
        returnOriginal: false,
      }
    );
  };

  static findByUsername = (username) => {
    return User.findOne({ username: username });
  };

  static findByEmail = (email) => {
    return User.findOne({ email: email });
  };

  static findByEmailOrUsername = (username) => {
    const user = User.findOne({
      $or: [{ email: username }, { username: username }],
    });
    return user;
  };
}

module.exports = userModel;

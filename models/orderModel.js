const db = require("../configs/mongo");
const Order = db.collection("orders");
const { ObjectId } = require("mongodb");

class orderModel {
  static find = () => {
    return Order.aggregate([
      {
        $lookup: {
          from: "packages",
          localField: "packageId",
          foreignField: "_id",
          as: "package",
        },
      },
      {
        $lookup: {
          from: "templates",
          localField: "templateId",
          foreignField: "_id",
          as: "template",
        },
      },
      {
        $unwind: {
          path: "$package",
        },
      },
      {
        $unwind: {
          path: "$template",
        },
      },
    ]).toArray();
  };

  static findOne = (id) => {
    return Order.aggregate([
      { $match: { _id: ObjectId(id) } },
      {
        $lookup: {
          from: "packages",
          localField: "packageId",
          foreignField: "_id",
          as: "package",
        },
      },
      {
        $lookup: {
          from: "templates",
          localField: "templateId",
          foreignField: "_id",
          as: "template",
        },
      },
      {
        $unwind: {
          path: "$package",
        },
      },
      {
        $unwind: {
          path: "$template",
        },
      },
    ]).toArray();
  };

  static insertOne = async (newOrder) => {
    const order = await Order.insertOne({
      ...newOrder,
      packageId: ObjectId(newOrder.packageId),
      templateId: ObjectId(newOrder.templateId),
    });
    return this.findOne(order.ops[0]._id);
  };

  static findOneAndDelete = (id) => {
    return Order.findOneAndDelete({ _id: ObjectId(id) });
  };

  static findOneAndUpdate = async (id, newOrder) => {
    await Order.findOneAndUpdate(
      { _id: ObjectId(id) },
      {
        $set: {
          ...newOrder,
          packageId: ObjectId(newOrder.packageId),
          templateId: ObjectId(newOrder.templateId),
        },
      },
      {
        returnOriginal: false,
      }
    );
    return this.findOne(id);
  };

  static findByDomain = (domain) => {
    return Order.findOne({ domain: domain });
  };

  static findByDomainAndActive = (domain) => {
    return Order.aggregate([
      { $match: { active: true } },
      { $match: { domain: domain } },
      {
        $lookup: {
          from: "packages",
          localField: "packageId",
          foreignField: "_id",
          as: "package",
        },
      },
      {
        $lookup: {
          from: "templates",
          localField: "templateId",
          foreignField: "_id",
          as: "template",
        },
      },
      {
        $unwind: {
          path: "$package",
        },
      },
      {
        $unwind: {
          path: "$template",
        },
      },
    ]).toArray();
  };

  static insertOneWish = async (orderId, wish) => {
    const order = await Order.findOneAndUpdate(
      { _id: ObjectId(orderId) },
      { $push: { "detail.wishes": wish } },
      { returnOriginal: false }
    );
    return this.findByDomainAndActive(order.value.domain);
  };
}
module.exports = orderModel;

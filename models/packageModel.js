const db = require("../configs/mongo");
const Package = db.collection("packages");
const { ObjectId } = require("mongodb");

class packageModel {
  static find = () => {
    return Package.aggregate([
      {
        $lookup: {
          from: "features",
          localField: "featureIds",
          foreignField: "_id",
          as: "features",
        },
      },
    ]).toArray();
  };

  static findActiveData = () => {
    return Package.aggregate([
      { $match: { active: true } },
      {
        $lookup: {
          from: "features",
          localField: "featureIds",
          foreignField: "_id",
          as: "features",
        },
      },
    ]).toArray();
  };

  static findOne = (id) => {
    return Package.aggregate([
      { $match: { _id: ObjectId(id) } },
      {
        $lookup: {
          from: "features",
          localField: "featureIds",
          foreignField: "_id",
          as: "features",
        },
      },
    ]).toArray();
  };

  static insertOne = async (newPackage) => {
    const packageData = await Package.insertOne(newPackage);
    return this.findOne(packageData.ops[0]._id);
  };

  static findOneAndDelete = (id) => {
    return Package.findOneAndDelete({ _id: ObjectId(id) });
  };

  static findOneAndUpdate = async (id, newPackage) => {
    await Package.findOneAndUpdate(
      { _id: ObjectId(id) },
      { $set: newPackage },
      {
        returnOriginal: false,
      }
    );
    return this.findOne(id);
  };
}
module.exports = packageModel;

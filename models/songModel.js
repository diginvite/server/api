const db = require("../configs/mongo");
const Song = db.collection("songs");

class songModel {
  static async insertMany(newSongs) {
    var songs = [];
    for (let i = 0; i < newSongs.length; i++) {
      const songChecker = await this.findByUrl(newSongs[i].url);
      if (songChecker === null) {
        const newSong = {
          title: newSongs[i].title,
          artist: newSongs[i].artist,
          url: newSongs[i].url,
          thumbnailUrl: newSongs[i].thumbnailUrl,
          path: newSongs[i].path,
        };
        const song = await this.insertOne(newSong);
        songs.push(song.ops[0]);
      } else {
        songs.push(songChecker);
      }
    }
    return songs;
  }

  static insertOne(newSong) {
    return Song.insertOne(newSong);
  }

  static find() {
    return Song.find().toArray();
  }

  static findByUrl(url) {
    return Song.findOne({ url: url });
  }
}

module.exports = songModel;

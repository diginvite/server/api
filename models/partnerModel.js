const db = require("../configs/mongo");
const Partner = db.collection("partners");
const { ObjectId } = require("mongodb");

class partnerModel {
  static find = () => {
    return Partner.find({}).toArray();
  };

  static findActiveData = (id) => {
    return Partner.find({ active: true }).toArray();
  };

  static findOne = (id) => {
    return Partner.findOne({ _id: ObjectId(id) });
  };

  static insertOne = (newPartner) => {
    return Partner.insertOne(newPartner);
  };

  static findOneAndDelete = (id) => {
    return Partner.findOneAndDelete({ _id: ObjectId(id) });
  };

  static findOneAndUpdate = (id, newPartner) => {
    return Partner.findOneAndUpdate(
      { _id: ObjectId(id) },
      { $set: newPartner },
      {
        returnOriginal: false,
      }
    );
  };
}

module.exports = partnerModel;

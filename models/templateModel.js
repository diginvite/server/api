const db = require("../configs/mongo");
const Template = db.collection("templates");
const { ObjectId } = require("mongodb");

class templateModel {
  static find = () => {
    return Template.aggregate([
      {
        $lookup: {
          from: "packages",
          localField: "packageId",
          foreignField: "_id",
          as: "package",
        },
      },
      {
        $unwind: {
          path: "$package",
        },
      },
    ]).toArray();
  };

  static findActiveData = () => {
    return Template.aggregate([
      { $match: { active: true } },
      {
        $lookup: {
          from: "packages",
          localField: "packageId",
          foreignField: "_id",
          as: "package",
        },
      },
      {
        $unwind: {
          path: "$package",
        },
      },
    ]).toArray();
  };

  static findOne = (id) => {
    return Template.aggregate([
      { $match: { _id: ObjectId(id) } },
      {
        $lookup: {
          from: "packages",
          localField: "packageId",
          foreignField: "_id",
          as: "package",
        },
      },
      {
        $unwind: {
          path: "$package",
        },
      },
    ]).toArray();
  };

  static insertOne = async (newTemplate) => {
    const template = await Template.insertOne({
      ...newTemplate,
      packageId: ObjectId(newTemplate.packageId),
    });
    return this.findOne(template.ops[0]._id);
  };

  static findOneAndUpdate = async (id, newTemplate) => {
    await Template.findOneAndUpdate(
      { _id: ObjectId(id) },
      { $set: { ...newTemplate, packageId: ObjectId(newTemplate.packageId) } },
      {
        returnOriginal: false,
      }
    );
    return this.findOne(id);
  };

  static findOneAndDelete = (id) => {
    return Template.findOneAndDelete({ _id: ObjectId(id) });
  };
}
module.exports = templateModel;

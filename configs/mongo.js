const { MongoClient } = require("mongodb");
const dbUrl = process.env.DB_URL || "mongodb://localhost:27017";
const client = new MongoClient(dbUrl, { useUnifiedTopology: true });
const dbName = process.env.DB_NAME || "diginviteDev";

// const connect = async () => await client.connect()
client.connect();

const db = client.db(dbName);
module.exports = db;

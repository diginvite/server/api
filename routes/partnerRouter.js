const router = require("express").Router();
const partnerController = require("../controllers/partnerController");

router.get("/", partnerController.find);
router.post("/", partnerController.insertOne);
router.get("/:partnerId", partnerController.findById);
router.put("/:partnerId", partnerController.findOneAndUpdate);
router.delete("/:partnerId", partnerController.findOneAndDelete);

module.exports = router;

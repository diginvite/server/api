const router = require("express").Router();
const featureController = require("../controllers/featureController");

router.get("/", featureController.find);
router.post("/", featureController.insertOne);
router.get("/:featureId", featureController.findById);
router.put("/:featureId", featureController.findOneAndUpdate);
router.delete("/:featureId", featureController.findOneAndDelete);

module.exports = router;

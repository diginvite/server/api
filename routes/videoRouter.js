const router = require("express").Router();
const videoController = require("../controllers/videoController");

router.get("/", videoController.find);
router.post("/", videoController.insertOne);

module.exports = router;

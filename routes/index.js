const router = require("express").Router();

const userRouter = require("./userRouter");
const songRouter = require("./songRouter");
const orderRouter = require("./orderRouter");
const videoRouter = require("./videoRouter");
const imageRouter = require("./imageRouter");
const featureRouter = require("./featureRouter");
const partnerRouter = require("./partnerRouter");
const packageRouter = require("./packageRouter");
const templateRouter = require("./templateRouter");
const webRouter = require("./webRouter");

const authController = require("../controllers/authController");

const {
  authenticateToken,
  authenticateUser,
} = require("../middlewares/authMiddleware");

router.post("/auth/login", authController.login);

router.use("/webs", webRouter);

router.use(authenticateToken);
router.use(authenticateUser);
router.use("/songs", songRouter);
router.use("/users", userRouter);
router.use("/orders", orderRouter);
router.use("/images", imageRouter);
router.use("/videos", videoRouter);
router.use("/packages", packageRouter);
router.use("/features", featureRouter);
router.use("/partners", partnerRouter);
router.use("/templates", templateRouter);

module.exports = router;

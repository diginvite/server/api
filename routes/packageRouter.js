const router = require("express").Router();
const packageController = require("../controllers/packageController");

router.get("/", packageController.find);
router.post("/", packageController.insertOne);
router.get("/:packageId", packageController.findById);
router.put("/:packageId", packageController.findOneAndUpdate);
router.delete("/:packageId", packageController.findOneAndDelete);

module.exports = router;

const router = require("express").Router();
const songController = require("../controllers/songController");

router.get("/", songController.find);
router.post("/", songController.insertOne);

module.exports = router;

const router = require("express").Router();
const orderController = require("../controllers/orderController");

router.get("/", orderController.find);
router.post("/", orderController.insertOne);
router.get("/:orderId", orderController.findById);
router.delete("/:orderId", orderController.findOneAndDelete);
router.put("/:orderId", orderController.findOneAndUpdate);

module.exports = router;

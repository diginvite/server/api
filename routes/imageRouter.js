const router = require("express").Router();
const imageController = require("../controllers/imageController");

router.get("/", imageController.find);
router.post("/", imageController.insertOne);

module.exports = router;

const router = require("express").Router();
const userController = require("../controllers/userController");

router.get("/findProfile", userController.findProfile);
router.put("/changePassword/:userId", userController.changePassword);
router.get("/", userController.find);
router.post("/", userController.insertOne);
router.get("/:userId", userController.findById);
router.delete("/:userId", userController.findOneAndDelete);
router.put("/:userId", userController.findOneAndUpdate);

module.exports = router;

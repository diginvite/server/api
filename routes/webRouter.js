const router = require("express").Router();
const webController = require("../controllers/webController");
const orderController = require("../controllers/orderController");

router.get("/findFeatures", webController.findFeatures);
router.get("/findPackages", webController.findPackages);
router.get("/findTemplates", webController.findTemplates);
router.get("/findPartners", webController.findPartners);
router.get("/findOrder", webController.findOrder);
router.post("/insertOneWish/:orderId", webController.insertOneWish);
router.post("/order", orderController.insertOne);

module.exports = router;

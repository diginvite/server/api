const router = require("express").Router();
const templateController = require("../controllers/templateController");

router.get("/", templateController.find);
router.post("/", templateController.insertOne);
router.get("/:templateId", templateController.findById);
router.put("/:templateId", templateController.findOneAndUpdate);
router.delete("/:templateId", templateController.findOneAndDelete);

module.exports = router;

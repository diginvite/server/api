const { morphism } = require("morphism");
const featureTranformer = require("./featureTransformer");

class packageTransformer {
  static requestTranform = (data) => {
    const schema = {
      name: "name",
      description: "description",
      active: "active",
      price: "price",
      priceDiscount: "priceDiscount",
      featureIds: "featureIds",
    };
    return morphism(schema, data);
  };

  static responseTranform = (data) => {
    const schema = {
      id: "_id",
      name: "name",
      description: "description",
      price: "price",
      priceDiscount: "priceDiscount",
      active: "active",
    };
    let packageTransformed = morphism(schema, data);
    packageTransformed.features = featureTranformer.responseTranform(
      data.features
    );
    return packageTransformed;
  };
}

module.exports = packageTransformer;

const { morphism } = require("morphism");

class videoTransformer {
  static requestTransform = (video) => {
    const schema = {
      url: "url",
      thumbnailUrl: "thumbnailUrl",
      path: "path",
      caption: "caption",
    };
    return morphism(schema, video);
  };

  static responseTransform = (image) => {
    const schema = {
      id: "_id",
      url: "url",
      thumbnailUrl: "thumbnailUrl",
      path: "path",
      caption: "caption",
    };
    return morphism(schema, image);
  };
}

module.exports = videoTransformer;

const { morphism } = require("morphism");

class featureTranformer {
  static requestTranform = (feature) => {
    const schema = {
      name: "name",
      description: "description",
      active: "active",
    };
    return morphism(schema, feature);
  };

  static responseTranform = (feature) => {
    const schema = {
      id: "_id",
      name: "name",
      description: "description",
      active: "active",
    };
    return morphism(schema, feature);
  };
}

module.exports = featureTranformer;

const { morphism } = require("morphism");

class imageTransformer {
  static requestTransform = (image) => {
    const schema = {
      url: "url",
      thumbnailUrl: "thumbnailUrl",
      path: "path",
    };
    return morphism(schema, image);
  };

  static responseTransform = (image) => {
    const schema = {
      id: "_id",
      url: "url",
      thumbnailUrl: "thumbnailUrl",
      path: "path",
    };
    return morphism(schema, image);
  };
}

module.exports = imageTransformer;

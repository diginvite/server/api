const { morphism } = require("morphism");
const packageTransformer = require("./packageTransformer");
const templateTransformer = require("./templateTransformer");

class orderTranformer {
  static responseTranform = (order) => {
    const schema = {
      id: "_id",
      name: "name",
      email: "email",
      description: "description",
      phone: "phone",
      domain: "domain",
      active: "active",
      detail: "detail",
    };

    let orderTransformed = morphism(schema, order);
    orderTransformed.package = packageTransformer.responseTranform(
      order.package
    );
    orderTransformed.template = templateTransformer.responseTranform(
      order.template
    );
    return orderTransformed;
  };

  static requestTranform = (order) => {
    const schema = {
      name: "name",
      description: "description",
      email: "email",
      phone: "phone",
      domain: "domain",
      active: "active",
      packageId: "package.id",
      templateId: "template.id",
      detail: "detail",
    };
    return morphism(schema, order);
  };
}

module.exports = orderTranformer;

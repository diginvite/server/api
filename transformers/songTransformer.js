const { morphism } = require("morphism");

class songTransformer {
  static requestTransform = (song) => {
    const schema = {
      url: "url",
      thumbnailUrl: "thumbnailUrl",
      path: "path",
      title: "title",
      artist: "artist",
    };
    return morphism(schema, song);
  };

  static responseTransform = (song) => {
    const schema = {
      id: "_id",
      url: "url",
      thumbnailUrl: "thumbnailUrl",
      path: "path",
      title: "title",
      artist: "artist",
    };
    return morphism(schema, song);
  };
}

module.exports = songTransformer;

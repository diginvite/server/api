const { morphism } = require("morphism");

class partnerTranformer {
  static requestTranform = (feature) => {
    const schema = {
      name: "name",
      description: "description",
      link: "link",
      image: "image",
      active: "active",
    };
    return morphism(schema, feature);
  };

  static responseTranform = (feature) => {
    const schema = {
      id: "_id",
      name: "name",
      description: "description",
      link: "link",
      image: "image",
      active: "active",
    };
    return morphism(schema, feature);
  };
}

module.exports = partnerTranformer;

const { morphism } = require("morphism");

class userTransformer {
  static responseTranform = (user) => {
    const schema = {
      id: "_id",
      fullName: "firstName",
      firstName: "firstName",
      lastName: "lastName",
      email: "email",
      username: "username",
      image: "image",
    };
    let userTransformed = morphism(schema, user);
    userTransformed.fullName =
      userTransformed.firstName + " " + userTransformed.lastName;
    return userTransformed;
  };

  static requestTranform = (user) => {
    const schema = {
      firstName: "firstName",
      lastName: "lastName",
      email: "email",
      username: "username",
      password: "password",
      image: "image",
    };
    return morphism(schema, user);
  };

  static requestUpdateTranform = (user) => {
    const schema = {
      firstName: "firstName",
      lastName: "lastName",
      email: "email",
      image: "image",
    };
    return morphism(schema, user);
  };
}

module.exports = userTransformer;

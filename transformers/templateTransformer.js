const { morphism } = require("morphism");
const packageTransformer = require("./packageTransformer");

class templateTranformer {
  static responseTranform = (template) => {
    const schema = {
      id: "_id",
      domain: "domain",
      name: "name",
      description: "description",
      active: "active",
      isPremium: "isPremium",
      images: "images",
    };
    let templateTransformed = morphism(schema, template);
    templateTransformed.package =
      template.package && packageTransformer.responseTranform(template.package);
    return templateTransformed;
  };

  static requestTranform = (template) => {
    const schema = {
      domain: "domain",
      name: "name",
      description: "description",
      active: "active",
      isPremium: "isPremium",
      packageId: "package.id",
      images: "images",
    };
    return morphism(schema, template);
  };
}

module.exports = templateTranformer;
